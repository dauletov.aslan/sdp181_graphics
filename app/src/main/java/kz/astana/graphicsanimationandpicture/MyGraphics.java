package kz.astana.graphicsanimationandpicture;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

public class MyGraphics extends View {

    private Paint paint;
    private Resources res;

    public MyGraphics(Context context) {
        super(context);
        paint = new Paint();
        res = getResources();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        paint.setColor(res.getColor(R.color.one));
        paint.setStyle(Paint.Style.FILL);
        canvas.drawPaint(paint);

        drawCircle(canvas);
        drawRect(canvas);
        drawText(canvas);
        rotate(canvas);
        drawBitmap(canvas);
    }

    private void drawCircle(Canvas canvas) {
        paint.setAntiAlias(true);
        paint.setColor(res.getColor(R.color.two));
        canvas.drawCircle(200, 200, 100, paint);
    }

    private void drawRect(Canvas canvas) {
        paint.setColor(res.getColor(R.color.three));
        canvas.drawRect(200, 400, 500, 600, paint);

        paint.setColor(res.getColor(R.color.seven));
        canvas.drawRoundRect(600, 400, 800, 600, 600, 400, paint);
    }

    private void drawText(Canvas canvas) {
        paint.setColor(res.getColor(R.color.four));
        paint.setTextSize(100f);
        canvas.drawText("Hello world!", 200, 700, paint);
    }

    private void rotate(Canvas canvas) {
        paint.setColor(res.getColor(R.color.five));
        paint.setTextSize(70f);
        canvas.rotate(45, 200, 900);
        canvas.drawText("Rotate", 200, 900, paint);
        canvas.rotate(-45);
    }

    private void drawBitmap(Canvas canvas) {
        Bitmap bitmap = BitmapFactory.decodeResource(res, R.drawable.arch);
        paint.setColor(res.getColor(R.color.six));
        canvas.drawBitmap(bitmap, 0, 1200, paint);
    }
}
