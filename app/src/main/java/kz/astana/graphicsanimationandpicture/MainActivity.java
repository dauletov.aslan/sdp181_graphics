package kz.astana.graphicsanimationandpicture;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private TextView animationTextView;
    private AnimationDrawable animationDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView firstImage = findViewById(R.id.firstImage);
        ImageView secondImage = findViewById(R.id.secondImage);

        try {
            InputStream inputStream = getAssets().open("arch.jpg");
            Drawable drawable = BitmapDrawable.createFromStream(inputStream, null);
            firstImage.setImageDrawable(drawable);
            inputStream.close();

            inputStream = getAssets().open("android.png");
            drawable = BitmapDrawable.createFromStream(inputStream, null);
            secondImage.setImageDrawable(drawable);
            inputStream.close();
        } catch (Exception e) {
            Log.e("Hello", e.toString());
        }

        Button graphics = findViewById(R.id.graphicsButton);
        graphics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GraphicsActivity.class);
                startActivity(intent);
            }
        });

        TextView textView = findViewById(R.id.textView);
        textView.setTypeface(Typeface.createFromAsset(getAssets(), "great_vibes_regular.otf"));

        animationTextView = findViewById(R.id.animationTextView);

        ImageView frameAnimation = findViewById(R.id.frameAnimation);
        frameAnimation.setBackgroundResource(R.drawable.frame_animation);
        animationDrawable = (AnimationDrawable) frameAnimation.getBackground();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_animation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Animation animation = null;
        switch (item.getItemId()) {
            case R.id.set:
                animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.set);
                break;
            case R.id.alpha:
                animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.alpha);
                break;
            case R.id.scale:
                animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.scale);
                break;
            case R.id.rotate:
                animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate);
                break;
            case R.id.translate:
                animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.translate);
                break;
            case R.id.frame_start:
                animationDrawable.start();
                return true;
            case R.id.frame_stop:
                animationDrawable.stop();
                return true;
        }
        animationTextView.startAnimation(animation);
        return true;
    }
}